vpc_cidr_block          = "10.0.0.0/16"
subnet_cidr_block       = "10.0.10.0/24"
avail_zone              = "ap-southeast-1a"
env_prefix              = "dev"
instance_type           = "t2.micro"

#replace your ipaddress below
my_ip                   = "ip-address/32"

#copy the public key from below location
# cat /root/.ssh/id_rsa.pub 
public_key_value        = "ssh-rsa ****"
#private_subnets_per_vpc = "3"
